﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSetActiveRope : MonoBehaviour
{

    private SpriteRenderer mSprRen;

    void Start()
    {
        mSprRen = this.GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "AppearRope")
        {
            if (mSprRen != null)
            {
                mSprRen.enabled = true;
                //Debug.Log("Xuat hien");

            }
        }

        else if (other.tag == "DisappearRope")
        {
            if (mSprRen != null)
            {
                mSprRen.enabled = false;
                //Debug.Log("Bien mat");

            }
        }
    }

}
