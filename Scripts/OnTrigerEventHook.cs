﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTrigerEventHook : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ground")
        {
            Character.EvtTrigerGround(this.gameObject);
        }
    }

}
