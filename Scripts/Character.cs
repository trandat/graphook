﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    [SerializeField]
    private GameObject _grapHookLeft;
    [SerializeField]
    private GameObject _grapHookRight;
    [SerializeField]
    private float _speedFire;

    [SerializeField]
    private float _speedPull;

    private LineRenderer _lineRenderer;
    // Start is called before the first frame update


    private GameObject mGrapHookLeft;
    private GameObject mGrapHookRight;

    private bool _canFire = true;
    private bool _left = true;
    private bool _right = false;

    public delegate void EventTrigerGround(GameObject mGameObject);

    public static EventTrigerGround EvtTrigerGround;

    void Start()
    {
        mGrapHookLeft = Instantiate(_grapHookLeft);
        mGrapHookLeft.transform.SetParent(this.transform, false);
        mGrapHookLeft.transform.localPosition = this.transform.position;


        mGrapHookRight = Instantiate(_grapHookRight);
        mGrapHookRight.transform.SetParent(this.transform, false);
        mGrapHookRight.transform.localPosition = this.transform.position;

        _lineRenderer = GetComponent<LineRenderer>();


        EvtTrigerGround += Handle_EventTrigerGroud;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (_canFire)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (_left)
                {
                    Go(mGrapHookLeft, _speedFire);
                    _canFire = false;
                    _right = true;
                    _left = false;
                    goto NEXT_TAG;
                }
                if (_right)
                {
                    Go(mGrapHookRight, _speedFire);
                    _canFire = false;
                    _right = false;
                    _left = true;
                    goto NEXT_TAG;
                }

            }
        }
        NEXT_TAG:
        if (_lineRenderer != null)
        {
            _lineRenderer.SetPosition(1, transform.position);
            _lineRenderer.SetPosition(0, mGrapHookLeft.transform.position);
            _lineRenderer.SetPosition(2, mGrapHookRight.transform.position);
        }
    }

    void Go(GameObject _GameObj, float _Speed)
    {
        Rigidbody2D mRigiBody2D = _GameObj.GetComponent<Rigidbody2D>();
        if (mRigiBody2D == null)
        {
            mRigiBody2D = _GameObj.AddComponent<Rigidbody2D>();
        }
        if(mRigiBody2D != null)
        {
            mRigiBody2D.gravityScale = 0;
            mRigiBody2D.velocity = _GameObj.transform.up * _Speed * Time.deltaTime;
        }
    }

    void CharacterGo(GameObject _GameObj, GameObject _HookObj, float _Speed)
    {
        Rigidbody2D mRigiBody2D = _GameObj.GetComponent<Rigidbody2D>();

        if (mRigiBody2D != null)
        {
            if (_HookObj == mGrapHookLeft)
            {
                mRigiBody2D.AddForce(new Vector2(-_Speed, _Speed));
                mRigiBody2D.AddForce(new Vector2(0, 0));
            }
            if (_HookObj == mGrapHookRight)
            {
                mRigiBody2D.AddForce(new Vector2(_Speed, _Speed));
                mRigiBody2D.AddForce(new Vector2(0, 0));
            }
        }
    }

    void Handle_EventTrigerGroud(GameObject mGameObject)
    {
        Go(mGameObject, 0);
        CharacterGo(this.gameObject, mGameObject, _speedPull);
        StartCoroutine("HookReturnCharacter", mGameObject);
    }

    IEnumerator HookReturnCharacter(GameObject mGameObject)
    {
        float mDistance = Vector3.Distance(mGameObject.transform.position, this.transform.position);
        yield return 0;
        while (mDistance > 0)
        {
            mGameObject.transform.position = Vector3.MoveTowards(mGameObject.transform.position, this.transform.position, _speedFire * Time.deltaTime);
            mDistance = Vector3.Distance(mGameObject.transform.position, this.transform.position);
            yield return 0;
        }
        mGameObject.transform.position = this.transform.position;
        Rigidbody2D mRigiBody2d = mGameObject.GetComponent<Rigidbody2D>();
        if (mRigiBody2d != null)
        {
            Destroy(mRigiBody2d);
        }
        _canFire = true;
        yield return 0;
        
    }

}
