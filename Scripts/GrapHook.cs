﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapHook : MonoBehaviour
{
    [SerializeField]
    private Sprite _hook;
    [SerializeField]
    private mChoiceDirection _choiceDirection;

    private enum mChoiceDirection
    {
        Left,
        Right
    }

    private float[] _direction = { 45, -45 };
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject mObjHook = this.gameObject;
        //Rigidbody2D mRigiHook = mObjHook.AddComponent<Rigidbody2D>();
        BoxCollider2D mBoxColHook = mObjHook.AddComponent<BoxCollider2D>();
        SpriteRenderer mSprRenHook = mObjHook.AddComponent<SpriteRenderer>();
        mObjHook.AddComponent<OnTrigerEventHook>();
        mSprRenHook.sprite = _hook;

        //mRigiHook.bodyType = RigidbodyType2D.Kinematic;
        //mRigiHook.gravityScale = 0;
        mBoxColHook.isTrigger = true;
        mBoxColHook.size = new Vector2(0.5F, 0.5F);
        mObjHook.transform.localPosition = this.transform.position;
        mObjHook.transform.SetParent(this.transform, false);
        mObjHook.transform.rotation = Quaternion.AngleAxis(_direction[(int)_choiceDirection], Vector3.forward);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
